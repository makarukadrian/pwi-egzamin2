import StringIO
import zipfile


class InMemoryZip(object):
    def __init__(self):
        self.in_memory_zip = StringIO.StringIO()


    def append(self, filename_in_zip, file_contents):
        zf = zipfile.ZipFile(self.in_memory_zip, "a", zipfile.ZIP_DEFLATED, False)
        zf.writestr(filename_in_zip, file_contents)
        for zfile in zf.filelist:
            zfile.create_system = 0
        return self

    def read(self):
        self.in_memory_zip.seek(0)
        return self.in_memory_zip.read()

    def writetofile(self, filename):
        f = file(filename, "w")
        f.write(self.read())
        f.close()
