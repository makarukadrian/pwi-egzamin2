# -*- coding: utf-8 -*-
from django.forms import ModelForm, HiddenInput, Textarea, PasswordInput
from django import forms
from .models import UserC
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class UzForm(ModelForm):
    class Meta:
        model = UserC
        fields = [
            'login', 'password'
        ]
        widgets = {
            'password': PasswordInput(),

        }

    def zapisz(self, commit = True):
        user = super(UzForm, self).save(commit = False)
        if commit:
            user.save()
        return user


class LoginForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'password'
        ]
        widgets = {
            'password' : PasswordInput(),
        }

class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required = True)

    class Meta:
        model = User
        fields = [
            'username', 'email', 'password1', 'password2'
        ]

        def save(self, commit = True):
            user = super(MyRegistrationForm, self).save(commit = False)
            user.email = self.cleaned_data['email']

            if commit:
                user.save()

            return user

    def cleane(self):
        email = self.cleaned_data['email']
        users = User.objects.all()
        for user in users:
            if email == user.email:
                raise forms.ValidationError(u"Uzyj innego maila")
        return email

    def cleanu(self):
        username = self.cleaned_data['username']
        dlugosc = len(username)
        if dlugosc < 4:
            raise forms.ValidationError(u"Login Conajmniej 4 znaki")
        return username

    def cleanp(self):
        password1 = self.cleaned_data['password1']
        dlugosc = len(password1)
        if dlugosc < 4:
            raise forms.ValidationError(u"Haslo conajmniej 4 znaki")
        return password1