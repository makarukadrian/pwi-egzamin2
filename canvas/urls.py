from django.conf.urls import patterns, url, include
from .views import *


#from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^register$', rejestracja, name='register'),
    url(r'^login$', zaloguj, name='login'),
    url(r'^logout$', wyloguj, name='logout'),
    url(r'^email/(?P<key>.+)$', aktywacja, name='email'),
    url(r'^oceny/$', oceny, name='oceny'),
    url(r'^pobierz/(?P<key>.+)$', pobierz, name='pobierz'),
    url(r'^uzytc/$', uzytc, name='uzytc'),
)