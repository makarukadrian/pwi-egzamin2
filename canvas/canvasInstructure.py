__author__ = 'Samsung'
#!/usr/bin/env python
import requests
import html5lib
from bs4 import BeautifulSoup as bs
import zipsklad
import redis
from rq.decorators import job
from memc import RedisCache

connect = redis.StrictRedis(host='pub-redis-10183.us-east-1-3.2.ec2.garantiadata.com', port=66666, db=0, password="db")

memc = RedisCache(connect)

url = {
    'login' : 'https://canvas.instructure.com/login',
    'courses' : 'https://canvas.instructure.com/courses',
    }

def login(username, password):
    payload = {
        'pseudonym_session[unique_id]': username,
        'pseudonym_session[password]': password
    }
    session = requests.session()
    session.post(url["login"], data=payload)
    return session

def courses(username, password):
    session = login(username, password)
    request = session.get(url["courses"])
    html = request.text
    parsed1 = bs(html, "html5lib")
    kursy = []
    id = []
    try:
        mu = parsed1.find_all('ul', {'class' : "menu-item-drop-column-list"})
        ul = mu[0]
        lis = ul.find_all('li', {'class' : 'customListItem'})
        for li in lis:
            id.append(li["data-id"])
        spans = ul.find_all('span', {'class' : 'name ellipsis'})
        i = 0
        for span in spans:
            kursy.append([span.findAll(text=True)[0], id[i]])
            i+=1
    except:
        return None
    return kursy

def oceny(username, password, course_id):
    session = login(username, password)
    request = session.get('https://canvas.instructure.com/courses/'+course_id+'/grades')
    html = request.text
    parsed1 = bs(html, "html5lib")
    typ = []
    id = dict()
    i = 0
    for typy in parsed1.find_all('tr', {'class' : 'student_assignment  hard_coded group_total'}):
        typ.append([])
        id[typy.findAll(text=True)[1].strip()] = i
        i+=1
    for f in parsed1.find_all('tr', {'class' : 'student_assignment assignment_graded editable'}):
        typ = f.find('div', {'class' : 'context'})
        id = id[typ.findAll(text=True)[0]]
        name = f.find('th', {'class' : 'title'}).a.findAll(text=True)[0]
        date = f.find('td', {'class' : 'due'}).findAll(text=True)[0].strip()
        score = f.find('span', {'class' : 'score'}).findAll(text=True)[0].split()[0]
        typ[id].append([name, date, score])
    for rodzaj in typ:
        rodzaj.sort(key=lambda x: x[1])
    return id, typ
@job('check', connection=connect)
def cache(key):
    return memc.get('zip_' + str(key))
@job('zip', connection=connect)
def zipsklad(key, ids, tablice):
    zippo = zipsklad.InMemoryZip()
    for id in ids:
        txt = ""
        for nazwa, data, wynik in tablice[ids[id]]:
            txt += nazwa+ "Uzyskane Punkty: "+wynik+" Date: "+ data + "\n"
        zippo.append(id+'.txt', txt.encode('utf-8'))
    read = zippo.read()
    memc.set('zip_' + str(key), read)
    return read