__author__ = 'Samsung'
import cPickle

class RedisCache(object):
    def __init__(self, redis, prefix='cache|', timeout=60*60):
        assert redis
        self._redis = redis
        self._prefix = prefix
        self._timeout = timeout

    def set(self, key, value, timeout=None):
        timeout = timeout or self._timeout
        key = self._prefix + key
        self._redis.pipeline().set(key, cPickle.dumps(value)).expire(key, timeout).execute()

    def get(self, key):
        data = self._redis.get(self._prefix + key)
        return (data and cPickle.loads(data)) or None

    def flush(self, pattern='', step=1000):
        keys = self._redis.keys(self._prefix + pattern + '*')
        [self._redis.delete(*keys[i:i+step]) for i in xrange(0, len(keys), step)]