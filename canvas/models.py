# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

class UserC(models.Model):
    user = models.OneToOneField(User)
    login = models.CharField(max_length=20)
    password = models.CharField(max_length=15)
    is_set = models.BooleanField()


class UserKey(models.Model):
    key = models.CharField(max_length=40)
    user = models.OneToOneField(User)
    is_active = models.BooleanField()