# -*- coding: utf-8 -*-

from .models import UserC, UserKey
from .forms import UzForm, LoginForm, MyRegistrationForm
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from uuid import uuid4 as uuid
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
import canvasInstructure

#def Tabela(request, posty):
#    tabela = []
#    for post in posty:
#        if (timezone.now() - post.data).seconds / 60 < 10:
#            if post.autor == request.user:
#                tabela.append(post.pk)
#        else:
#            break
#    return tabela


def index(request):
    info = request.session.get('info', None)
    if info:
        del request.session['info']
    return render(request, 'canvas/main.html', {"info" : info})

def rejestracja(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            WyslijEmail(request.POST['username'])
            request.session['info'] = u'Zarejestrowales sie, potwierdz utworzenie konta przez email.'
            return HttpResponseRedirect(reverse('index'))
    else:
        form = MyRegistrationForm()
    return render(request, "canvas/rejestracja.html", {'form': form})

def zaloguj(request):
    info = request.session.get('info', None)
    if request.method == 'GET':
        if request.user.is_authenticated():
            request.session['info'] = u'Jestes zalogowany'
            return HttpResponseRedirect(reverse('index'))
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'canvas/logowanie.html', {"info" : info, 'form' : form})
    elif request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        try:
            is_activte = UserKey.objects.get(user=user).is_active
        except:
            is_activte = False
        if user and is_activte:
            login(request, user)
            request.session['info'] = u'Zalogowales sie'
            return HttpResponseRedirect(reverse('index'))
        elif is_activte:
            info = u'Blad w danych'
        else:
            info = u'Musisz potwierdzic utworzenie konta'
        form = LoginForm()
        form.fields['username'].help_text = None
        return render(request, 'canvas/logowanie.html', {"info" : info, 'form' : form})

def wyloguj(request):
    logout(request)
    request.session['info'] = u'Wylogowales sie'
    return HttpResponseRedirect(reverse('index'))




def aktywacja(request, key):
    uk = UserKey.objects.get(key=key)
    uk.is_active = True
    uk.save()
    request.session['info'] = u'Twoje konto zostalo aktywowane.'
    return HttpResponseRedirect(reverse('index'))

def WyslijEmail(username):
    user = User.objects.get(username=username)
    key = uuid().hex
    uk = UserKey(key=key, user=user, is_active=False)
    uk.save()
    userc = UserC(user = user, login='', password='', is_set=False)
    userc.save()
    od = 'zadanie@egzamin.pl'
    do = user.email
    subject = 'Aktywacja konta'
    message = 'Aby aktywowac konto prosimy o klikniecie w ponizszy link:\n' \
    'Link : ' + 'http://mysite-zadpwi.rhcloud.com/canvas/email/' + key
    send_mail(subject, message, od, [do])


@login_required(login_url='/login')
def uzytc(request):
    if request.method == 'GET':
        form = UzForm()
        return render(request, 'canvas/uzytC.html', {'form' : form})
    else:
        form = UzForm(request.POST)
        s = form.save(commit=False)
        login = s.login
        password = s.password
        userc = UserC.objects.get(user=request.user)
        userc.login = login
        userc.password = password
        userc.is_set = True
        userc.save()
        return HttpResponseRedirect(reverse('index'))

def UserCanvas(request):
    try:
        userc = UserCanvas.objects.get(user=request.user)
    except:
        userc = None
    return userc

@login_required(login_url='/login')
def oceny(request):
    userc = UserCanvas(request)
    if userc and userc.is_set:
        courses = canvasInstructure.courses(userc.login, userc.password)
        return render(request, 'canvas/oceny.html', {'courses' : courses})
    else:
        return HttpResponseRedirect(reverse('uzytC'))

@login_required(login_url='/login')
def pobierz(request, key):
    userc = UserCanvas(request)
    if userc and userc.is_set:
        zipsklad = canvasInstructure.cache(key)
        print('Zzzzipowane ' + str(zipsklad))
        if zipsklad is None:
            ids, tablice = canvasInstructure.oceny(userc.login, userc.password, key)
            zip = canvasInstructure.zipsklad(key, ids, tablice)
        response = HttpResponse(zipsklad, content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=oceny' + str(key) + '.zip'
        return response

